function Hamburger(size, stuffing) {
    if (size === Hamburger.SIZE_SMALL || size === Hamburger.SIZE_LARGE) {
        this.size = size;
    } else {
        throw new HamburgerException("Вы неправильно указали размер!");
    }

    if (stuffing === Hamburger.STUFFING_CHEESE || stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO) {
        this.stuffing = stuffing;
    } else {
        throw new HamburgerException("Вы неправильно указали добавку!");
    }
}

Hamburger.prototype.addTopping = function (topping) {
    if (topping === Hamburger.TOPPING_MAYO || topping === Hamburger.TOPPING_SPICE) {
        this.topping = topping;
    } else {
        throw new HamburgerException("Вы неправильно указали поливку!");
    }
}

Hamburger.prototype.removeTopping = function (topping) {
    if (topping || this.topping === topping) {
        this.topping = undefined;
    } else {
        throw new HamburgerException("Эта поливка не установлена!");
    }
}

Hamburger.prototype.calculatePrice = function () {
    if (this.topping === undefined) {
        return this.size.price + this.stuffing.price;
    } else {
        return this.size.price + this.stuffing.price + this.topping.price;
    }
}

Hamburger.prototype.calculateCalories = function () {
    if (this.topping === undefined) {
        return this.size.calories + this.stuffing.calories;
    } else {
        return this.size.calories + this.stuffing.calories + this.topping.calories;
    }
}

Hamburger.prototype.getToppings = function () {
    return {
        toppings: {mayo: Hamburger.TOPPING_MAYO, spice: Hamburger.TOPPING_SPICE}
    }
}

Hamburger.prototype.getStuffings = function () {
    return {
        stuffing: {
            cheese: Hamburger.STUFFING_CHEESE,
            salad: Hamburger.STUFFING_SALAD,
            potato: Hamburger.STUFFING_POTATO,
        },
    }
}

Hamburger.prototype.getSize = function () {
    return {
        size: {small: Hamburger.SIZE_SMALL, large: Hamburger.SIZE_LARGE},
    }
}

Hamburger.prototype.getSize = function () {
    return {
        size: {small: Hamburger.SIZE_SMALL, large: Hamburger.SIZE_LARGE},
    }
}

function HamburgerException(message) {
    this.message = message;
}

HamburgerException.prototype = Object.create(TypeError.prototype)


/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {price: 50, calories: 20};
Hamburger.SIZE_LARGE = {price: 100, calories: 40};
Hamburger.STUFFING_CHEESE = {price: 20, calories: 40};
Hamburger.STUFFING_SALAD = {price: 20, calories: 5};
Hamburger.STUFFING_POTATO = {price: 15, calories: 10};
Hamburger.TOPPING_MAYO = {price: 20, calories: 5};
Hamburger.TOPPING_SPICE = {price: 15, calories: 0};

try {
    var hamb = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    hamb.addTopping(Hamburger.TOPPING_MAYO);
    hamb.addTopping(Hamburger.TOPPING_SPICE);
    hamb.removeTopping(Hamburger.TOPPING_MAYO);

    console.log(hamb);
    console.log("Price: ", hamb.calculatePrice());
    console.log("Calories: ", hamb.calculateCalories());


    console.log(hamb.getToppings());
    console.log(hamb.getSize());
    console.log(hamb.getStuffings());
} catch (err) {
    console.log(err);
}



